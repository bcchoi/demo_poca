# python3

import os
import sys
import json, time, datetime, requests, pyaudio, wave, random, time
from openframeworks_extra import *
import urllib.request
from tinydb import TinyDB, Query
import speech_recognition as sr
from konlpy.tag import Twitter
from konlpy.utils import pprint
from flask import Flask, request
import threading
from pythonosc import osc_message_builder
from pythonosc import udp_client

app = Flask('server')

filename = 'word.mp3'
naver_client_id = 'YoVNcxTJK4F9CjjIB0nO'
naver_client_secret = 'o04mQin8Gf'

class ofApp(object):
	callback = None
	db = TinyDB('db.json')
	twitter = Twitter()
	client = udp_client.SimpleUDPClient('127.0.0.1', 12347)
	def __init__(self):
		ret = self.twitter.morphs(u'아버지가방에들어가신다')
		print('ret ', ret)

		@app.route('/cart_update', methods=['GET', 'POST'])
		def cart_update():
			print('cart_update')
			print(request.json)
			self.client.send_message('/demo', ["/worker", "cart_update", json.dumps(request.json)])
			time.sleep(1)
			return 'OK'

		t1 = threading.Thread(target=self._start_server)
		t1.start()

	def create_user(self, name, confidence):
		return self.db.insert({'name': name, 'confidence': confidence})

	def update_user(self, name, order):
		User = Query()
		return self.db.update({'order':order}, User.name == name)

	def get_user(self, name):
		User = Query()
		t = self.db.search(User.name == name)
		return json.dumps(t)

	def parsing_speech(self, word):
		word = self.twitter.morphs(word)
		print(word)
		if '체크' in word:
			return 1
		if any(st in ['전등', '운동', '전동'] for st in word):
			if any(st in ['켜', ['켜기'], '거죠'] for st in word):
				return 2
		if '에어컨' in word:
			return 3
		if any(st in ['커피', '주문', '모드'] for st in word):
			return 4
		if any(st in ['커튼', '가드', '내려'] for st in word):
			return 5
		if any(st in ['문', '닫아'] for st in word):
			return 6
		if '테스트' in word:
			return 7
		return 0

	def speech(self, word):
		encText = urllib.parse.quote(word)
		data = "speaker=mijin&speed=0&text=" + encText
		url = "https://openapi.naver.com/v1/voice/tts.bin"
		request = urllib.request.Request(url)
		request.add_header("X-Naver-Client-Id", naver_client_id)
		request.add_header("X-Naver-Client-Secret", naver_client_secret)
		response = urllib.request.urlopen(request, data=data.encode('utf-8'))
		rescode = response.getcode()
		if (rescode == 200):
			response_body = response.read()
			with open('data/'+filename, 'wb') as f:
				f.write(response_body)
		else:
			print("Error Code:" + rescode)
		return filename

	def detect_face(self, file):
		url = "https://openapi.naver.com/v1/vision/face" # 얼굴감지
		files = {'image': open(file, 'rb')}
		headers = {'X-Naver-Client-Id': naver_client_id, 'X-Naver-Client-Secret': naver_client_secret}
		data1 = datetime.datetime.now()
		response = requests.post(url, files=files, headers=headers)
		data2 = datetime.datetime.now()
		diff = data2 - data1
		print(diff.total_seconds())
		rescode = response.status_code
		if (rescode == 200):
			print(response.text)
			return response.text
		else:
			print("Error Code:" + rescode)

	def celebrity_face(self, file):
		url = "https://openapi.naver.com/v1/vision/celebrity"  # 유명인 얼굴인식
		files = {'image': open(file, 'rb')}
		headers = {'X-Naver-Client-Id': naver_client_id, 'X-Naver-Client-Secret': naver_client_secret}
		data1 = datetime.datetime.now()
		response = requests.post(url, files=files, headers=headers)
		data2 = datetime.datetime.now()
		diff = data2 - data1
		print(diff.total_seconds())
		rescode = response.status_code
		if (rescode == 200):
			print(response.text)
			return response.text
		else:
			print("Error Code:" + rescode)

	def recognize_speech(self):
		r = sr.Recognizer()
		with sr.Microphone() as source:
			print("Say something!")
			audio = r.listen(source, phrase_time_limit=2)
		print('audio done')
		try:
			# for testing purposes, we're just using the default API key
			# to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
			# instead of `r.recognize_google(audio)`
			t = r.recognize_google(audio, language='ko-KR')
			print(t)
			return t
			#print t.encode('cp949')
			#return t.encode('utf8')

		# print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
		except sr.UnknownValueError:
			print("Google Speech Recognition could not understand audio")
		except sr.RequestError as e:
			print("Could not request results from Google Speech Recognition service; {0}".format(e))


	def record_audio(self):
		CHUNK = 1024
		FORMAT = pyaudio.paInt16
		CHANNELS = 2
		RATE = 44100
		RECORD_SECONDS = 5
		WAVE_OUTPUT_FILENAME = "apt.wav"

		p = pyaudio.PyAudio()

		stream = p.open(format=FORMAT,
						channels=CHANNELS,
						rate=RATE,
						input=True,
						frames_per_buffer=CHUNK)

		print('* Recording audio...')

		frames = []

		for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
			data = stream.read(CHUNK)
			frames.append(data)

		print('* done')

		stream.stop_stream()
		stream.close()
		p.terminate()

		wf = wave.open('data/' + WAVE_OUTPUT_FILENAME, 'wb')
		wf.setnchannels(CHANNELS)
		wf.setsampwidth(p.get_sample_size(FORMAT))
		wf.setframerate(RATE)
		wf.writeframes(b''.join(frames))
		wf.close()

		return WAVE_OUTPUT_FILENAME

	def set_callback(self, func):
		self.callback = func

	def _start_server(sefl):
		app.run(threaded = True)


