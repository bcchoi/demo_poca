#pragma once

#include "ofMain.h"
#include "fsm/state_context.h"	// Context

class ofApp : public ofBaseApp{
	// state
	itg::ofxStateMachine<Context> stateMachine;
public:
	void setup();
	void update();
	void draw();
	void exit();
	void keyPressed(int key);

	void changeState(std::string state) {
		stateMachine.changeState(state);
	}
};
