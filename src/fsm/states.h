/*
by bcc
2017.06.27
*/

#pragma once
#include "state_context.h"

template <typename A, typename B>
multimap<B, A> flip_map(map<A, B> & src) {

	multimap<B, A> dst;

	for (map<A, B>::const_iterator it = src.begin(); it != src.end(); ++it)
		dst.insert(pair<B, A>(it->second, it->first));

	return dst;
}

class ofxUserState : public itg::ofxState<Context> {
protected:
public:
	virtual string getName() { return ""; };
	virtual void stateEnter() {
		Context& ctx = getSharedData();
		ofLogNotice("current state") << getName();
		ctx.currentState = getName();
	}
};

/*
ReadyFaceFile -> SaveFaceImage -> Idle
					^		 |
					|10 time |
					|________|
*/
class ReadyFaceFile: public ofxUserState {
	int delay;
public:
	string getName() { return "readyfacefile"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		// delay = ofGetElapsedTimeMillis();
		// ctx.speech(L"사진을 촬영합니다.");
	}
	void update() {
		// if (ofGetElapsedTimeMillis() - delay <= 2000) return;

		Context& ctx = getSharedData();
		ctx.folder = ofToString(ofGetUnixTime());
		ofDirectory::createDirectory(ctx.folder);
		ofDirectory dir(ofToDataPath(ctx.folder, true));
		
		changeState("savefaceimage");
	}

};

class SaveFaceImage : public ofxUserState {
	int save_image_timeout;
	int idx;	
public:
	string getName() { return "savefaceimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
		idx = 0;		
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - save_image_timeout > 100) {
			save_image_timeout = ofGetElapsedTimeMillis();
			if (ctx.realsense.getNumTrackedFaces() > 0) {
				ofPixels pixels = ctx.realsense.getColorPixelsRef();
				ofRectangle rt(ctx.faceRect.x, ctx.faceRect.y, ctx.faceRect.width, ctx.faceRect.height);				
				rt.scaleFromCenter(2.0f);				
				if (rt.x > 0 && rt.y > 0 && rt.x + rt.width < pixels.getWidth() && rt.y + rt.height < pixels.getHeight()) {
					pixels.crop(rt.x, rt.y, rt.width, rt.height);
					pixels.resize(rt.width*0.5, rt.height*0.5);
					std::string filename = ctx.folder + "/" + ctx.folder + "g" + ofToString(idx++) + ".jpg";
					ofSaveImage(pixels, filename);
					ofLogNotice() << filename;	
					std::string ret = ctx.detect_face("data/" + filename);
					ofxJSONElement body;
					body.parse(ret);
					for (int i = 0; i < body["faces"].size(); i++) {
						ofxJSONElement pose = body["faces"][i]["pose"];
						std::string s = pose["value"].asString();
						if (s == "frontal_face") {
							ctx.face_result = ret;
							ctx.face_filename = filename;							
							if (ctx.sinario == 4) {
								changeState("frontal_face");
							}
							else {
								ofSaveImage(pixels, "C:\\Users\\bcc\\gitwork\\test\\test.jpg");	// for openface							
								ctx.sendOpen("classifier");								
								changeState("manager");
							}							
							return;
						}
						else {
							// delete file	
							std::string removefname = ofToDataPath(filename, true);
							ofFile::removeFile(removefname);
						}
					}
				}
			}
		}
	}
};


class FrontalFace : public ofxUserState {
	int delay;
public:
	string getName() { return "frontal_face"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {		
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();	
	}
	void update() {
		Context& ctx = getSharedData();
		ofxJSONElement body;
		body.parse(ctx.face_result);
		for (int i = 0; i < body["faces"].size(); i++) {	
			ofxJSONElement gender = body["faces"][i]["gender"];
			ofxJSONElement age = body["faces"][i]["age"];
			ofxJSONElement emotion = body["faces"][i]["emotion"];

			std::wostringstream w;
			if (gender["value"].asString() == "male") w << L"남성 ";
			else w << L"여성 ";
			std::string str = age["value"].asString();
			std::wstring wstr = L"";
			wstr.assign(str.begin(), str.end());
			w << wstr << L"세 ";
			str = emotion["value"].asString();
			wstr.assign(str.begin(), str.end());
			w << wstr << L" 입니다";
			ctx.speech(w.str());
			ctx.effector((int)ofRandom(0, 7));
			ctx.daemonChangeState("recommand", 3500);
			return;
		}
	}
};

class Recommand : public ofxUserState {
	int delay; // for ment
	ofx::TaskQueue queue;	// recommand site
public:
	string getName() { return "recommand"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();
		ctx.ment("recommand");
		ctx.effector((int)ofRandom(0, 7));
		queue.start(new SystemTask("start chrome -kiosk -fullscreen https://pocarobot.myshopify.com/collections/recommand"));
	}
	void update() {
		Context& ctx = getSharedData();		
		if (ofGetElapsedTimeMillis() - delay <= 3000) return;
		changeState("celebrity");
	}
};

class Manager : public ofxUserState {
	int delay;	// for ment
public:
	string getName() { return "manager"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		ctx.isManager = false;		
		ctx.speech(L"관리자인지 확인중입니다.");
		ctx.effector((int)ofRandom(0, 7));
	}
	void update() {
		Context& ctx = getSharedData();

		if (ctx.isManager) {	// by Context's _receiver
			std::wostringstream w;			
			if (ctx.identity == "bcc" && ctx.confidence >= 0.80f) {
				w << L"관리자가 맞습니다.";
				ctx.sendCafe("/test", 1);
				ctx.delayChangeState("iotmode", 4500);
			}				
			else {
				w << L"관리자가 아닙니다.";
				ctx.sinario = 4;
				ctx.sendCafe("/test", ctx.sinario);
				ctx.delayChangeState("idle", 4500);
			}
			w << int(ctx.confidence * 100) << L" 퍼센트 정확도입니다.";
			ctx.speech(w.str());
			ctx.effector((int)ofRandom(0, 7));			
		}
	}
};

class IOTMode : public ofxUserState {
	int delay;	// for ment
public:
	string getName() { return "iotmode"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();
		ctx.ment("iot_mode");
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= 2000) return;
		changeState("mic");
	}
};

class Celebrity : public ofxUserState {
	int delay; // for ment
	ofx::TaskQueue queue;		// ordered menu site
public:
	string getName() { return "celebrity"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();		
		ctx.ment("check_come_before");
		ctx.effector((int)ofRandom(0, 7));
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= 2000) return;	
		std::map<std::wstring, double> person;
		ofDirectory dir;
		dir.listDir(ofToDataPath(ctx.folder, true));
		bool firstuser = true;
		double maxconfidence = 0.0f;
		for (int i = 0; i < dir.numFiles(); i++) {
			ctx.face_filename = ctx.folder + "/" + dir.getFile(i).getFileName();
			std::string ret = ctx.celebrite("data/" + ctx.face_filename);
			ofxJSONElement body;			
			body.parse(ret);			
			for (int i = 0; i < body["faces"].size(); i++) {
				ofxJSONElement celebrity = body["faces"][i]["celebrity"];
				double confidence = celebrity["confidence"].asDouble();
				std::wstring value = ctx.utf8ToUnicode(celebrity["value"].asString().c_str());
				person[value] = confidence;
		
				if (confidence > maxconfidence) {
					maxconfidence = confidence;
				}
				char* utf8 = ctx.unicodeToUtf8(value.c_str());
				ofxPythonObject func = ctx.script.attr("get_user");
				if (func) {
					ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
					if (obj) {
						std::string ret = obj.asString();
						ofxJSONElement body;
						body.parse(ret);
						if (body.empty()) {							
							ofLog() << "empty";							
						}
						else {
							for (int i = 0; i < body.size(); i++) {								
								ofLog() << "not empty";
								firstuser = false;								
							}
						}
					}
				}
			}
		}

		std::wostringstream w;
		if (firstuser)	w << L"처음 오셨네요.";						
		else w << L"또 오셨네요.";
		w << int(maxconfidence * 100) << L" 퍼센트 정확도입니다.";
		if (firstuser) {
			w << L"메뉴를 선택해 주세요.";
		}
		else {
			w << L" 이전에 주문하신 메뉴로 이동합니다.";
			queue.start(new SystemTask("start chrome -kiosk -fullscreen https://pocarobot.myshopify.com/collections/frontpage/products/product-6"));
		}
		ctx.speech(w.str());
		ctx.effector((int)ofRandom(0, 7));		
		for (std::map<std::wstring, double>::iterator it = person.begin(); it != person.end(); ++it) {
			std::wstring tmp = it->first;
			char* utf8 = ctx.unicodeToUtf8(tmp.c_str());
			ofxPythonObject func = ctx.script.attr("get_user");
			if (func) {
				ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
				if (obj) {
					std::string ret = obj.asString();
					ofxJSONElement body;
					body.parse(ret);
					if (body.empty()) {					
						ofxPythonObject func = ctx.script.attr("create_user");						
						if (func && it->second >= 0.4) {
							ofxPythonObject obj = func(ofxPythonObject::fromString(utf8), ofxPythonObject::fromFloat(it->second));
							if (obj) {
								int ret = obj.asInt();
								ofLog() << "created user:" << ret;
							}
						}
					}
				}
			}
		}
		ctx.delayChangeState("menu", 4000);
	}
};

class Menu : public ofxUserState {
public:
	string getName() { return "menu"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		ctx.effector((int)ofRandom(0, 7));
	}
};

class Checkout : public ofxUserState {
public:
	string getName() { return "checkout"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void update() {
		Context& ctx = getSharedData();
		ctx.sendSerial("printer", "4902408317526276=22132011419431400000");
		ctx.delayChangeState("goodbye", 2000);
	}
};

class Goodbye : public ofxUserState {
	int delay;	// for ment
public:
	string getName() { return "goodbye"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();
		ctx.speech(L"결제가 완료 되었습니다. 이용해 주셔서 감사 합니다.");
		ctx.effector((int)ofRandom(0, 7));
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= 4000) return;
		changeState("tmp");
	}
};

class FaceDetected : public ofxUserState {
	int delay;	// for ment
public:
	string getName() { return "facedetected"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();		
		ctx.ment("lookatme");
		ctx.effector((int)ofRandom(0, 7));
	}
	void update() {
		Context& ctx = getSharedData();		
		if (ofGetElapsedTimeMillis() - delay <= 2000) return;		
		changeState("readyfacefile");
	}
};

class Mic : public ofxUserState {
	ofSoundPlayer wink;
public:
	string getName() { return "mic"; }
	void setup() {
		ofLogNotice("loading") << getName();
		wink.load("sound/wink.wav");
	}
	void update() {
		Context& ctx = getSharedData();
		wink.play();
		ctx.effectorNoSound(1);
		ofxPythonObject func = ctx.script.attr("recognize_speech");
		if (func) {
			ofxPythonObject obj = func();
			if (obj) {
				std::string utf8 = obj.asString();
				
				ofxPythonObject func = ctx.script.attr("parsing_speech");
				if (func) {
					ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
					if (obj) {
						if (obj.asInt() > 0) {
							ctx.sinario = obj.asInt();
							changeState("iot");
							return;
						}
						else {
							ctx.ment("unknown_command");
							ctx.effectorNoSound((int)ofRandom(7,10));
						}
					}
				}
			}
		}		
		ctx.delayChangeState("mic", 2000); // unknown...
	}
};

// call by context
class IOT: public ofxUserState {
	int delay; // for ment
public:
	string getName() { return "iot"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		Context& ctx = getSharedData();
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();
		if (ctx.sinario == 1) ctx.ment("check_in");
		else if (ctx.sinario == 2) ctx.ment("lamp_on");
		else if (ctx.sinario == 3) ctx.ment("air");
		else if (ctx.sinario == 4) ctx.ment("order_coffee");
		else if (ctx.sinario == 5) ctx.ment("bliend");
		else if (ctx.sinario == 6) ctx.ment("door");
		else if (ctx.sinario == 7) ctx.ment("speech_test");
		ctx.sendCafe("/test", ctx.sinario);
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= 2500) return;
		ctx.effectorNoSound((int)ofRandom(0, 7));
		// send tablet
		ofLog() << "send tablet............";

		if (ctx.sinario == 4) changeState("idle");
		else {			
			ctx.delayChangeState("mic", 10);
		}
	}
};

// call by context
class Daemon : public ofxUserState {
	int delay;
	int save_image_timeout;
	int idx;
public:
	string getName() { return "daemon"; }
	void setup() {
		ofLogNotice("loading") << getName();
		idx = 0;
	}
	void stateEnter() {		
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();		
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= ctx.delay_time) {

			if (ofGetElapsedTimeMillis() - save_image_timeout > ctx.delay_time/5) {
				save_image_timeout = ofGetElapsedTimeMillis();
				if (ctx.realsense.getNumTrackedFaces() > 0) {
					ofPixels pixels = ctx.realsense.getColorPixelsRef();
					ofRectangle rt(ctx.faceRect.x, ctx.faceRect.y, ctx.faceRect.width, ctx.faceRect.height);
					rt.scaleFromCenter(1.5f);
					if (rt.x > 0 && rt.y > 0 && rt.x + rt.width < pixels.getWidth() && rt.y + rt.height < pixels.getHeight()) {
						pixels.crop(rt.x, rt.y, rt.width, rt.height);
						pixels.resize(rt.width*0.5, rt.height*0.5);
						std::string filename = ctx.folder + "/" + ctx.folder + "g" + ofToString(idx++) + ".jpg";
						ofSaveImage(pixels, filename);
						ofLogNotice() << filename;
					}
				}
			}
			return;
		}
		changeState(ctx.targetState);
	}
};

// call by context
class Delay : public ofxUserState {
	int delay;
public:
	string getName() { return "delay"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		delay = ofGetElapsedTimeMillis();
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= ctx.delay_time) return;
		changeState(ctx.targetState);
	}
};

class Idle : public ofxUserState {	
public:
	string getName() { return "idle"; }
	void setup() {
		ofLogNotice("loading") << getName();	
		Context& ctx = getSharedData();
		ofxJSONElement json;
		json["pan"] = 0;
		json["tilt"] = -35;
		ctx.sendSerial("motor", json.getRawString());
	}	
};

class Init : public ofxUserState {
	int delay;	// for ment
public:
	string getName() { return "init"; }
	void setup() {
		ofLogNotice("loading") << getName();		
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();		
		ctx.ment("start_ment");
		ctx.effector(9);
		delay = ofGetElapsedTimeMillis();
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - delay <= 2000) return;
		changeState("idle");								
	}
};


class Tmp : public ofxUserState {
	int delay;
public:
	string getName() { return "tmp"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
};
