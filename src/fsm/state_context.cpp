
/*
by bcc
2017.06.27
*/
#include "state_context.h"
#include "ofApp.h"
#include "ofxPythonCallBack.h"


class CustomCallBack : public ofxPythonCallBack {
public:
	void call(ofxPythonObject args, ofxPythonObject kwargs) {
		std::string tmp = ofToString(args.asVector()[0].asVector()[0].asFloat());
		ofLog() << tmp;
	}
};
void Context::setup() {
	make_image_timeout = 0;
	currentState = "";

	_receiver.setup(SELFPORT);
	_openSender.setup("192.168.99.100", OPENPORT);
	_cafeSender.setup("192.168.0.20", CAFEPORT);		// external address
	_serialSender.setup("localhost", SERIALPORT);

	realsense.setup(true, false, true, false, true); // grab color, and use textures
	realsense.open(); // opens device

	// dc motor
	panPos = 0;	tiltPos = 0;	

	// python
	python.init();
	python.executeScript("worker.py");
	ofxPythonObject klass = python.getObject("ofApp");
	script = klass();

	faces[0].load("images/ExpectationFace.png");
	faces[1].load("images/HappyFace.png");
	faces[2].load("images/JoyFace.png");
	faces[3].load("images/LaughFace.png");
	faces[4].load("images/WinkFace.png");	
	faces[5].load("images/PleasureFace.png");
	faces[6].load("images/BaseFace.png");
	faces[7].load("images/SadFace.png");
	faces[8].load("images/AngryFace.png");
	faces[9].load("images/SupriseFace.png");
	faceIdx = 6;

	soundFiles.push_back("expectation.wav");
	soundFiles.push_back("happy.wav");
	soundFiles.push_back("joy.wav");
	soundFiles.push_back("laugh.wav");
	soundFiles.push_back("wink.wav");	
	soundFiles.push_back("pleasure.wav");
	soundFiles.push_back("happy.wav");
	soundFiles.push_back("sadness.wav");
	soundFiles.push_back("angry.wav");
	soundFiles.push_back("suprise.wav");

	menutype = "";
	queue.start(new SystemTask("start chrome -kiosk -fullscreen https://pocarobot.myshopify.com/"));	
	queue.start(new SystemTask(ofToDataPath("ngrok.exe", true) + " http -subdomain=pocarobot 5000"));
	sinario = 0;
}
void Context::update() {
	ofApp* ofapp = (ofApp*)app;
	while (_receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		_receiver.getNextMessage(m);
		std::string address = m.getAddress();
		std::string from = m.getArgAsString(0);
		std::string cmd = m.getArgAsString(1);
		std::string value = m.getArgAsString(2);

		if (address == "/demo") {
			if (from == "/open") {
				if (cmd == "result") {
					ofLog() << value;
					std::vector<std::string> ret = ofSplitString(value, "#");
					if (ret.size() == 2) {
						isManager = true;
						identity = ret[0];
						confidence = ofToDouble(ret[1]);
					}
				}
			}
			else if (from == "/worker") {
				if (cmd == "cart_update" && currentState == "menu") {
					ofxJSONElement body;
					body.parse(value);
					std::vector<std::string> ii;
					int price = 0;
					for (int i = 0; i < body["line_items"].size(); i++) {
						std::string tmp = body["line_items"][i]["title"].asString();
						std::string ptm = body["line_items"][i]["price"].asString();
						price += ofToInt(ptm);
						ii.push_back(tmp);
					}
					
					std::wostringstream w;
					if (items.size() == ii.size()) {
						// checkout
						ofLog() << "checkout";
						w << price << L"�� ���� ��Ź�帳�ϴ�";		
						menutype = "checkout";
						delayChangeState("checkout", 10000);						
					}
					else if (items.size() > ii.size()) {
						// remove
						if (items.size()) {
							std::wstring title = utf8ToUnicode(items[items.size()-1].c_str());
							w << title << L"�� �����մϴ�.";
						}
						ofLog() << "remove";
						menutype = "remove";
					}
					else if (items.size() < ii.size()) {
						// add
						if (ii.size()) {							
							std::wstring title = utf8ToUnicode(ii[0].c_str());
							w << title << L"�� �����ϼ̱���";
						}
						ofLog() << "add";
						menutype = "add";
					}

					speech(w.str());

					// update
					items.clear();
					for (int i = 0; i < ii.size(); i++) {
						items.push_back(ii[i]);
					}
				}
			}
		}
	}	
	realsense.update();
	std::vector<int> max;
	for (int i = 0; i < realsense.getNumTrackedFaces(); i++) {
		const ofxRSFace* face = realsense.getTrackedFace(i);
		const PXCRectI32& outR = face->getFaceRect();
		max.push_back(outR.w * outR.h);
	}

	std::vector<int>::iterator result;
	result = std::max_element(max.begin(), max.end());
	int at = std::distance(max.begin(), result);
		
	if (realsense.getNumTrackedFaces() > 0) {
		if (currentState == "idle")
			ofapp->changeState("facedetected");

		if (max.size() == 0) at = 0;
		const ofxRSFace* face = realsense.getTrackedFace(at);
		const PXCRectI32& outR = face->getFaceRect();
		ofPoint center;
		center.x = outR.x + (outR.w / 2);
		center.y = outR.y + (outR.h / 2);

		faceRect.set(outR.x, outR.y, outR.w, outR.h);

		if (ofGetElapsedTimeMillis() - posUpdate > 100) {
			posUpdate = ofGetElapsedTimeMillis();
			if (center.x > 0 && center.y > 0) {
				bool sended = false;
				if (center.x < (FW / 2) - (WG * 3)) {
					if (panPos < 50) {
						panPos += 1;
						sended = true;
					}
				}
				if (center.x >(FW / 2) + (WG * 3)) {
					if (panPos > -50) {
						panPos -= 1;
						sended = true;
					}
				}
				if (center.y < (FH / 2) - (HG * 3)) {
					if (tiltPos > -15) {
						tiltPos -= 1;
						sended = true;
					}
				}
				if (center.y > (FH / 2) + (HG * 3)) {

					if (tiltPos < 25) {
						tiltPos += 1;
						sended = true;
					}
				}

				if (sended) {
					sended = false;
					ofxJSONElement json;
					json["pan"] = panPos;
					json["tilt"] = tiltPos;
					//sendSerial("motor", json.getRawString());
					//ofLogNotice() << "pan: " << panPos << ", tilt: " << tiltPos;
				}
			}
		}
	}
}
void Context::draw() {
#ifdef NDEBUG
	faces[faceIdx].draw(0, 0, 1024, 768);
#else
	faces[faceIdx].draw(0, 0, 1024, 768);
	int w = FW / 3;
	int h = FH / 3;
	realsense.drawColor(0, 0, w, h); // top right

	std::vector<int> max;
	for (int i = 0; i < realsense.getNumTrackedFaces(); i++) {
		const ofxRSFace* face = realsense.getTrackedFace(i);
		const PXCRectI32& outR = face->getFaceRect();
		max.push_back(outR.w * outR.h);
	}

	std::vector<int>::iterator result;
	result = std::max_element(max.begin(), max.end());
	int at = std::distance(max.begin(), result);

	if (realsense.getNumTrackedFaces() > 0) {

		if (max.size() == 0) at = 0;
		const ofxRSFace* face = realsense.getTrackedFace(at);
		const PXCRectI32& outR = face->getFaceRect();
		ofPoint center;
		center.x = outR.x + (outR.w / 2);
		center.y = outR.y + (outR.h / 2);

		ofPushStyle();
		ofNoFill();
		ofSetLineWidth(3);
		ofSetColor(ofColor::aqua);
		ofDrawCircle(center.x / 3, center.y / 3, 5);
		ofDrawRectangle(outR.x / 3, outR.y / 3, outR.w / 3, outR.h / 3);

		ofRectangle rt(outR.x / 3, outR.y / 3, outR.w / 3, outR.h / 3);
		rt.scaleFromCenter(1.5f);
		ofSetColor(ofColor::antiqueWhite);
		ofDrawRectangle(rt);
		ofPopStyle();
	}

	ofPushStyle();
	ofSetColor(127, 127, 255, 255);
	ofDrawLine(w / 2 - WG, 0, w / 2 - WG, h);
	ofDrawLine(w / 2 + WG, 0, w / 2 + WG, h);

	ofSetColor(255, 127, 255, 255);
	ofDrawLine(0, h / 2 - HG, w, h / 2 - HG);
	ofDrawLine(0, h / 2 + HG, w, h / 2 + HG);

	ofPopStyle();
#endif
}
void Context::sendOpen(std::string cmd, std::string value) {
	ofxOscMessage m;
	m.setAddress("/open");
	m.addStringArg("/demo");
	m.addStringArg(cmd);
	m.addStringArg(value);
	_openSender.sendMessage(m);
}
void Context::sendCafe(std::string cmd, int value) {
	ofxOscMessage m;
	m.setAddress("/test");
	m.addIntArg(value);
	_cafeSender.sendMessage(m);
}
void Context::sendSerial(std::string cmd, std::string value) {
	ofxOscMessage m;
	m.setAddress("/serial");
	m.addStringArg("/demo");
	m.addStringArg(cmd);
	m.addStringArg(value);
	_serialSender.sendMessage(m);
}


void Context::keyPressed(int key) {
	ofApp* ofapp = (ofApp*)app;	
	if (key == 'a') {	
		sinario = 0;		// init state
		ofapp->changeState("idle");
	}
	else if (key == 'b') {
		sendOpen("classifier");
	}
	else if (key == 'c') {
		detect_face("1499411250/1499411250g1.jpg");
	}
	else if (key == 'd') {
		ofapp->changeState("celebrity");
	}
	else if (key == 'e') {
		std::wstring tmp = L"�ֺ�ö";
		char* utf8 = unicodeToUtf8(tmp.c_str());
		ofxPythonObject func = script.attr("create_user");
		if (func) {
			ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
			if (obj) {
				std::string ret = obj.asString();				
			}
		}
	}
	else if (key == 'f') {
		std::wstring tmp = L"�ʶ��";
		char* utf8 = unicodeToUtf8(tmp.c_str());
		ofxPythonObject func = script.attr("get_user");
		if (func) {
			ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
			if (obj) {
				std::string ret = obj.asString();
				ofxJSONElement body;
				body.parse(ret);
				if (body.empty()) {
					ofLog() << "empty";
					ofxPythonObject func = script.attr("create_user");
					if (func) {
						ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
						if (obj) {
							int ret = obj.asInt();
							ofLog() << "created user:" << ret;
						}
					}
				}
				else {
					for (int i = 0; i < body.size(); i++) {
						std::string tmp = body[i]["name"].asString();
						std::wstring wstr = utf8ToUnicode(tmp.c_str());
						ofLog() << "not empty";
					}					
				}
			}
		}
	}
	else if (key == 'g') {
		std::string filename = ofToDataPath("1499667936/1499667936g4.jpg", true);
		ofFile::removeFile(filename);
	}
	else if (key == 'h') {
		ofxPythonObject func = script.attr("record_audio");
		if (func) {
			ofxPythonObject obj = func();
			if (obj) {
				std::string ret = obj.asString();
			}
		}
	}
	else if (key == 'i') {
		ofxPythonObject func = script.attr("recognize_speech");
		if (func) {
			ofxPythonObject obj = func();
			if (obj) {
				std::string ret = obj.asString();
				ofxPythonObject func = script.attr("speech");
				if (func) {
					ofxPythonObject obj = func(ofxPythonObject::fromString(ret));
					if (obj) {
						std::string t = obj.asString();
						speaker.load(t);
						speaker.play();
					}
				}
			}
		}
	}
	else if (key == 'j') {
		ofxPythonObject func = script.attr("recognize_speech");
		if (func) {
			ofxPythonObject obj = func();
			if (obj) {
				std::string ret = obj.asString();
				ofxPythonObject func = script.attr("parsing_speech");
				if (func) {
					ofxPythonObject obj = func(ofxPythonObject::fromString(ret));
					if (obj) {
						int t = obj.asInt();
						if (t == 1) {
							speech(L"������� �մϴ�");
						}
						else if (t == 2) {
							speech(L"������� ���ϴ�");
						}
						if (t == 3) {
							speech(L"������ �մϴ�");
						}
						else if (t == 4) {
							speech(L"������ ���ϴ�");
						}
					}
				}
			}
		}
	}
	else if (key == 'k') {
		sendOpen("classifier");
	}
	else if (key == 'l') {
		sendCafe("test", (int)ofRandom(0, 7));
	}
	else if (key == 'm') {
		ofxPythonObject func = script.attr("set_callback");
		if (func) {
			CustomCallBack * cb0 = new CustomCallBack();			
			func(CallBack2Python(cb0));
			ofLog() << "set_callback";
		}
	}
}
void Context::speech() {
	int t = ofGetElapsedTimeMillis();
	char* utf8 = unicodeToUtf8(speech_text.c_str());
	ofxPythonObject func = script.attr("speech");
	if (func) {
		ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
		if (obj) {
			std::string t = obj.asString();
			speaker.load(t);
			speaker.play();
		}
	}
	ofLogNotice() << "speech elapsed: " << ofGetElapsedTimeMillis() - t << "(msec)";
}
std::string Context::detect_face(std::string filename) {
	ofxPythonObject func = script.attr("detect_face");
	if (func) {
		ofxPythonObject obj = func(ofxPythonObject::fromString(filename));
		if (obj) {
			std::string r = obj.asString();
			return r;
		}
	}
	return "";
}

std::string Context::celebrite(std::string filename) {
	ofxPythonObject func = script.attr("celebrity_face");
	if (func) {
		ofxPythonObject obj = func(ofxPythonObject::fromString(filename));
		if (obj) {
			std::string r = obj.asString();
			return r;
		}
	}
	return "";
}
void Context::speech(std::string utf8) {
	int t = ofGetElapsedTimeMillis();
	ofxPythonObject func = script.attr("speech");
	if (func) {
		ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
		if (obj) {
			std::string t = obj.asString();
			speaker.load(t);
			speaker.play();
		}
	}
	ofLogNotice() << "speech elapsed: " << ofGetElapsedTimeMillis() - t << "(msec)";
}
void Context::speech(std::wstring unicode) {
	int t = ofGetElapsedTimeMillis();
	char* utf8 = unicodeToUtf8(unicode.c_str());
	ofxPythonObject func = script.attr("speech");
	if (func) {
		ofxPythonObject obj = func(ofxPythonObject::fromString(utf8));
		if (obj) {
			std::string t = obj.asString();
			speaker.load(t);
			speaker.play();
		}
	}
	ofLogNotice() << "speech elapsed: " << ofGetElapsedTimeMillis() - t << "(msec)";
}
void Context::ment(std::string filename) {
	sp2.loadSound("ments/"+filename+".mp3");
	sp2.play();
}
void Context::daemonChangeState(std::string state, int time) {
	ofApp* ofapp = (ofApp*)app;
	delay_time = time;
	targetState = state;
	ofapp->changeState("daemon");
}
void Context::delayChangeState(std::string state, int time) {
	ofApp* ofapp = (ofApp*)app;
	delay_time = time;
	targetState = state;
	ofapp->changeState("delay");
}
char* Context::unicodeToUtf8(const wchar_t* unicode) {
	char* strUtf8 = new char[256];
	memset(strUtf8, 0, 256);
	int nLen = WideCharToMultiByte(CP_UTF8, 0, unicode, lstrlenW(unicode), NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_UTF8, 0, unicode, lstrlenW(unicode), strUtf8, nLen, NULL, NULL);
	return strUtf8;
}
wchar_t* Context::utf8ToUnicode(const char* utf8) {
	wchar_t* strUnicode = new wchar_t[256];
	memset(strUnicode, 0, 256);
	char	strUTF8[256] = { 0, };
	strcpy_s(strUTF8, 256, utf8);
	int nLen = MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), NULL, NULL);
	MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), strUnicode, nLen);
	return strUnicode;
}
void Context::effector(int idx) {	
	sp.loadSound("sound/"+soundFiles[idx]);
	sp.play();
	sendSerial("led", ofToString(idx));
	faceIdx = idx;
}
void Context::effectorNoSound(int idx) {	
	sendSerial("led", ofToString(idx));
	faceIdx = idx;
}
void Context::exit() {
	realsense.close();
}