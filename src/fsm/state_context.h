

/*
by bcc
2017.06.27
*/

#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxStateMachine.h"
#include "ofxRealSense.h"
#include "ofxJSON.h"
#include "ofxPython.h"
#include "ofxTaskQueue.h"

#define SELFPORT 12347
#define OPENPORT 22352	// openface model
#define CAFEPORT 5679	// android tablet
#define SERIALPORT 12348


#define FW 1920
#define FH 1080
#define WG 60		
#define HG 30		

class SystemTask : public Poco::Task {
	std::string cmd;
public:
	SystemTask(std::string cmd) :Poco::Task("systemtask") {
		this->cmd = cmd;
	}
	void runTask() {
		system(cmd.c_str());
	}
};

class Context {
	ofxOscReceiver _receiver;
	ofxOscSender _openSender;
	ofxOscSender _cafeSender;	// simulation
	ofxOscSender _serialSender;	// led, motor, printer		

public:
	void setup();
	void update();
	void draw();
	void exit();
	void keyPressed(int key);

	// system
	ofBaseApp* app;
	std::string currentState;
	std::string targetState;
	ofRectangle faceRect;
	int delay_time;	// using the Delay state

	//make_face_image_
	int make_image_timeout;
	
	// folder name;
	std::string folder;

	// realsense
	ofxRealSense realsense;

	// dc motor
	int panPos;
	int tiltPos;
	int posUpdate;
	
	void sendOpen(std::string cmd, std::string value = "");
	void sendCafe(std::string cmd, int value = 0);
	void sendSerial(std::string cmd, std::string value = "");
	
	// python
	ofxPython python;
	ofxPythonObject script;
	ofx::TaskQueue queue;

	// tts
	ofSoundPlayer speaker;
	void speech(std::string utf8);
	void speech(std::wstring unicode);
	void speech();
	char* unicodeToUtf8(const wchar_t* unicode);
	wchar_t* utf8ToUnicode(const char* utf8);
	std::wstring speech_text;
	void daemonChangeState(std::string state, int time);
	void delayChangeState(std::string state, int time);

	// detect & celebrite face
	std::string detect_face(std::string filename);
	std::string celebrite(std::string filename);
	std::string face_result;
	std::string face_filename;

	// recognize speech
	int sinario;

	// openface
	double confidence = 0.0f;
	bool isManager = false;	
	std::string identity = "";

	// face & sound
	ofImage faces[10];
	int faceIdx;	
	ofSoundPlayer sp;
	ofSoundPlayer sp2;
	std::vector<string> soundFiles;
	void effector(int idx);
	void effectorNoSound(int idx);

	// menu
	std::vector<std::string> items;
	std::string menutype;

	// ment	
	void ment(std::string filename);
};
