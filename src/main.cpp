#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main() {

#ifdef NDEBUG											
	/* added for multi view*/
	ofGLFWWindowSettings settings;
	settings.setGLVersion(4, 1);
	settings.width = 1920;
	settings.height = 1080;
	settings.multiMonitorFullScreen = true;
	settings.decorated = false;
	ofCreateWindow(settings);
	/* end */
#else
	ofSetupOpenGL(300, 300, OF_WINDOW);			// <-------- setup the GL context
#endif
												// this kicks off the running of my app
												// can be OF_WINDOW or OF_FULLSCREEN
												// pass in width and height too:
	ofRunApp(new ofApp());

}
