#include "ofApp.h"
#include "fsm/states.h"

#define WG 60
#define HG 30
//--------------------------------------------------------------
void ofApp::setup(){
	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofBackground(0);
#ifdef NDEBUG
	ofSetWindowPosition(1930, 0);	
#else
	ofSetWindowPosition(640 + 60, 10);
#endif
	ofSetWindowShape(1024, 768);
	stateMachine.getSharedData().app = this;
	stateMachine.getSharedData().setup();	
	stateMachine.addState<FaceDetected>();
	stateMachine.addState<Idle>();
	stateMachine.addState<Init>();
	stateMachine.addState<Tmp>();
	stateMachine.addState<ReadyFaceFile>();
	stateMachine.addState<SaveFaceImage>();
	stateMachine.addState<Celebrity>();
	stateMachine.addState<Manager>();
	stateMachine.addState<FrontalFace>();
	stateMachine.addState<Daemon>();
	stateMachine.addState<Delay>();
	stateMachine.addState<Mic>();
	stateMachine.addState<IOT>();
	stateMachine.addState<Menu>();
	stateMachine.addState<IOTMode>();
	stateMachine.addState<Recommand>();
	stateMachine.addState<Checkout>();
	stateMachine.addState<Goodbye>();
	stateMachine.changeState("init");
}

//--------------------------------------------------------------
void ofApp::update() {
	stateMachine.getSharedData().update();
}

//--------------------------------------------------------------
void ofApp::draw(){
	stateMachine.getSharedData().draw();	
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){	
	stateMachine.getSharedData().keyPressed(key);
}

void ofApp::exit() {
	stateMachine.getSharedData().exit();
}
